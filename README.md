# Summary

The User Agent Page Killer module prevents requests from user agents that match a
pattern from completing a request. This module was developed for a
specific use case involving SSO and how it interacts with Microsoft Office apps.
The details of this problem can be seen on this microsoft [issue.](https://docs.microsoft.com/en-us/office/troubleshoot/office-suite-issues/click-hyperlink-to-sso-website#hyperlinks-from-office-to-internet-explorer-or-to-another-web-browser)

When a link is clicked from an office document, the office application recognizes
that the link is being redirected to the SSO login page. Instead of opening the
original url the office application attempts to open the redirect url. This
happens even if the redirect is a temporary redirect or a 302. This often causes
the user to recieve an access denined error instead of the original url. The functionality
of this module prevents certain user agents from being redirected by killing the
request before the redirect happens. This forces Office to use the original url
as intended. This module's code is based on this [stack exchange post.](https://stackoverflow.com/questions/2653626/why-are-cookies-unrecognized-when-a-link-is-clicked-from-an-external-source-i-e)



# REQUIREMENTS

No additional modules are explicitly required for user_agent_page_kill to function.

# INSTALLATION

* Install as usual, see https://www.drupal.org/documentation/install/modules-themes/modules-8
  for further information.

# SETUP

The module comes with a User agent pattern that is specific to office applications. If
The original use case for the module is not what you need then you should remove this
user agent configuration and supply your own.
Install and enable the module as normal then you can configure your user agent patterns here
/admin/config/services/user-agent-page-kill

# Warning
If your user agent pattern is too broad it might lead to a situation where all visitors
to the site are blocked. If this happens you can disable the functionality of the module
by disabling the module through drush.

`drush pmu user_agent_page_kill`

Or by deleting the configuration for the module.

`drush cdel user_agent_page_kill.settings`

Or you can set the configuration to an arbitrary value.

`drush cset user_agent_page_kill.settings user_agent_patterns 'foo'`
