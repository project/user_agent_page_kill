<?php

namespace Drupal\user_agent_page_kill\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Component\Utility\Xss;

/**
 * Event Subscriber that kills connections for a UserAgent.
 */
class UserAgentPageKiller implements EventSubscriberInterface {

  /**
   * Kill cache switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $cacheKillSwitch;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Creates a new EventSubscriber.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The page cache kill switch.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config Factory.
   */
  public function __construct(KillSwitch $kill_switch, ConfigFactory $configFactory) {
    $this->cacheKillSwitch = $kill_switch;
    $this->configFactory = $configFactory;
  }

  /**
   * Code that should be triggered on event specified.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event.
   */
  public function onRequest(GetResponseEvent $event) {
    $userAgent = $event->getRequest()->headers->get('user-agent');
    $userAgentPatterns = $this->configFactory->get('user_agent_page_kill.settings')->get('user_agent_patterns');
    $userAgentPatterns = implode('|', preg_split('/[\r\n\s\t]+/', $userAgentPatterns, -1, PREG_SPLIT_NO_EMPTY));
    if ($userAgentPatterns) {
      $filterUserAgent = Xss::filter($userAgent);
      if (preg_match("/{$userAgentPatterns}/i", $filterUserAgent)) {
        // Prevent killed pages from being cached.
        $this->cacheKillSwitch->trigger();
        die();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest', 400];
    return $events;
  }

}
