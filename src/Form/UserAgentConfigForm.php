<?php

namespace Drupal\user_agent_page_kill\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * UserAgent Config form.
 */
class UserAgentConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_agent_page_kill_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_agent_page_kill.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['user_agent_patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('User Agent REGEX patterns'),
      '#default_value' => $this->config('user_agent_page_kill.settings')->get('user_agent_patterns'),
      '#rows' => 10,
      '#weight' => 3,
      '#description' => $this->t('Enter Regex Patterns to identify User Agents. Enter one per line'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration, set values and save config.
    $this->configFactory->getEditable('user_agent_page_kill.settings')
      ->set('user_agent_patterns', $form_state->getValue('user_agent_patterns'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
